using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundMixerManager : MonoBehaviour
{

    [SerializeField] private AudioMixer audioMixer;

    public void SetMasterVolume(float level)
    {
        //audioMixer.SetFloat("MasterVol", level);
        audioMixer.SetFloat("MasterVol", Mathf.Log10(level) * 20f);
    }

    public void SetMusicVolume(float level)
    {
        //audioMixer.SetFloat("MusicVol", level);
        audioMixer.SetFloat("MusicVol", Mathf.Log10(level) * 20f);
    }

    public void SetSoundFXVolume(float level)
    {
        //audioMixer.SetFloat("SoundFX", level);
        audioMixer.SetFloat("SoundFX", Mathf.Log10(level) * 20f);
    }
}
