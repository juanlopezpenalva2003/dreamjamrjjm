using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip clickSound;
    public AudioClip hoverSound;

    public Animator anim;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play()
    {
        StartCoroutine(PlayNum());
    }
    IEnumerator PlayNum()
    {
        anim.SetTrigger("playAnim");
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("GameplayScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void OnClick()
    {
        SoundFXManager.instance.PlaySoundFXClip(clickSound, transform, 1f);
    }

    public void OnHover()
    {
        SoundFXManager.instance.PlaySoundFXClip(hoverSound, transform, 1f);
    }

    public void MMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
