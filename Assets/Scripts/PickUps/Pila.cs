using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pila : MonoBehaviour
{
    public EnergyBar enerB;
    public AudioClip pickPILA;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            IncreaseEnergy();
        }
    }

    public void IncreaseEnergy()
    {
        SoundFXManager.instance.PlaySoundFXClip(pickPILA, transform, 0.7f);
        enerB.energy += 25;
        gameObject.SetActive(false);
    }
}
