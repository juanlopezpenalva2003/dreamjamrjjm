using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    CharacterMovement charMov;

    public float energy;
    float maxEnergy;

    public Slider energyBar;
    public float dValue;
    // Start is called before the first frame update
    void Start()
    {
        maxEnergy = energy;
        energyBar.maxValue = maxEnergy;
        charMov = GetComponent<CharacterMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (energy > maxEnergy)
        {
            energy = maxEnergy;
        }
        DecreaseEnergy();
        energyBar.value = energy;

        if (energy <= 0)
        {
            charMov.Die();
        }
    }

    private void DecreaseEnergy()
    {
        if (energy != 0)
        {
            if (charMov.isRunningFast)
            {
                energy -= (dValue * 2) * Time.deltaTime;
            }else if (charMov.isRunning)
            {
                energy -= dValue * Time.deltaTime;
            }
        }
    }
}
