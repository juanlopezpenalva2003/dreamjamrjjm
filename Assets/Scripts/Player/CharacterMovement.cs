using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public DeathScreen deathScr;
    CharacterController chController;
    public float slowRunSpeed = 4f;
    public float fastRunSpeed = 7f;
    public float speed;
    public float jumpSpeed = 8f;
    public float gravity = 20f;
    public bool isJumping;
    public bool isRunning;
    public bool isRunningFast;
    public float airJumps = 1f;
    float currAJumps = 0f;

    public bool isDead = false;

    public AudioClip jump;
    public AudioClip die;

    public Animator anim;

    Vector3 moveDirection = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        chController = GetComponent<CharacterController>();
        speed = slowRunSpeed;
        isRunning = false;
        isRunningFast = false;
        isJumping = false;
    }

    // Update is called once per frame
    void Update()
    {

            if (chController.isGrounded)
            {
                anim.SetBool("isJumping", false);
                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
                isJumping = false;
                currAJumps = 0f;
                moveDirection *= speed;

                if (Input.GetButton("Jump"))
                {
                    isJumping = true;
                    anim.SetBool("isJumping", true);
                    SoundFXManager.instance.PlaySoundFXClip(jump, transform, 0.55f);
                    moveDirection.y = jumpSpeed;
                }
            }
            else if (!chController.isGrounded) {
                if (Input.GetButtonDown("Jump") && (currAJumps < airJumps) && (isJumping))
                {
                    currAJumps++;
                    SoundFXManager.instance.PlaySoundFXClip(jump, transform, 0.55f);
                    anim.SetTrigger("airJump");
                    moveDirection.y = jumpSpeed;
                }

                moveDirection.x = Input.GetAxis("Horizontal") * speed;

            }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            isRunningFast = true;
            anim.SetBool("isRunningFast", true);
            speed = fastRunSpeed;
        }
        else
        {
            isRunningFast = false;
            anim.SetBool("isRunningFast", false);
            speed = slowRunSpeed;
        }

        if (Input.GetAxis("Horizontal") != 0)
            {

                isRunning = true;
                anim.SetBool("isRunning", true);

                float xScale = Mathf.Abs(transform.localScale.x);
                if (Input.GetAxis("Horizontal") < 0)
                {
                    transform.localScale = new Vector3(-xScale, transform.localScale.y, transform.localScale.z);
                }
                else
                {
                    transform.localScale = new Vector3(xScale, transform.localScale.y, transform.localScale.z);
                }


            }
            else
            {
                isRunning = false;
                anim.SetBool("isRunning", false);
            }

        if (!isDead)
            chController.Move(moveDirection * Time.deltaTime);
         
            moveDirection.y -= gravity * Time.deltaTime;
        }
        

    public void Die()
    {
        SoundFXManager.instance.PlaySoundFXClip(die, transform, 0.55f);
        isDead = true;
        anim.SetBool("isDead", true);
        deathScr.gameObject.SetActive(true);
    }

}
